
- go mod 使用
  - 下载依赖包 go mod download
  - 环境变量设置 export GO111MODULE=on;go env -w GOPROXY=https://goproxy.cn,direct
- 编译
  - go build 
- 执行
  - sftpgo [command]
   1.    initprovider Initializes the configured data provider
   2.    portable     Serve a single directory
   3.    serve        Start the SFTP Server
